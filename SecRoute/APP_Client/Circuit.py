class Circuit:
    '''
    The Circuit object represenst a spesific circuit - a path built from 3 nodes.

    Attributes:
        info (json): The message's content
        successor_sock (socket) : The socket to send to
        private_key (bytes) : The key used to encrypt / decrypt this connection
        direction (int) : 1 = client -> server or 2 = server -> clinet
        is_server (bool) : if the connection is with the server
    '''

    def __init__(self):
        self.guard_node = None
        self.middle_node = None
        self.exit_node = None


    def calculate_circuit():
        pass # TODO
