import json
import sys
import base64
import Crypto_Utils
import Client_Logic_Unit

DIFFIE_HELLMAN_CODE = 0
PROCESS_AND_FORWARD_CODE = 1
SERVER_STATIC_ADDRESS = 9009

class TOR_Logic_Unit:
    '''
    The Application TOR Logic Unit is responsible to perform all the operations that are connected 
    to the TOR network.
    Encrypt, Decrypt Or Key - Exchange via Diffie Hellman. 

    The class is built in the singleton design pattern.

    Attributes:
        __TOR_msg: The to send to the server
        __CIRCUIT: Dict of the nodes' IPs, secret keys and open ports 
    '''

    __instance = None
    __CIRCUIT = {}  # {(IP, port): [private_key, shared_key]}
    __TOR_msg = {}
    __app_logic_unit = Client_Logic_Unit.Client_Logic_Unit()

    def __new__(cls):
        
        if (cls.__instance == None):
            cls.__instance = super(TOR_Logic_Unit, cls).__new__(cls)

        return cls.__instance


    """
    The function calculates a path of three nodes.
    param nodes_list : The table of the nodes' data
    type nodes_list : list
    """
    # nodes_list = [{id, ip, port, is_guard,
    # is_available, family: ["IP"]}]
    # circuit_obj = {"guard": (IP, open_port), ...}
    @staticmethod
    def calculate_circuit(nodes_list):
        
        circuit_family = []
        subnets = []
        
        circuit = {
            "guard" : None,
            "middle" : None,
            "exit" : None
        }
        
        for node in nodes_list:
        
            is_node_already_in_circuit = (node["IP"], node["open_port"]) in circuit.values()
            is_node_in_circuit_family = (node["IP"], node["open_port"]) in circuit_family
            node_subnet = ".".join(node["IP"].split(".", 2)[:2])
            #DOCKER - ADD is_IP_in_subnet = node_subnet in subnets
            chosen = False
            
            # first choose exit node
            if not circuit["exit"] and not (is_node_already_in_circuit or is_node_in_circuit_family
                or node["is_guard"]) and node["is_available"]:
                
                circuit["exit"] = (node["IP"], node["open_port"])
                chosen = True
        
        # second choose guard node
            elif not circuit["guard"] and not (is_node_already_in_circuit or is_node_in_circuit_family) \
                and node["is_guard"] and node["is_available"]:
                
                circuit["guard"] = (node["IP"], node["open_port"])
                chosen = True
        
        # last choose middle node       
            elif not circuit["middle"] and not (is_node_already_in_circuit or is_node_in_circuit_family or
                                                node["is_guard"]) and node["is_available"]:
                circuit["middle"] = (node["IP"], node["open_port"])
                chosen = True
            
            if chosen:
                circuit_family += node["family"]
                subnets += [node_subnet]
        
        return circuit


    def load_circuit(self, circuit_obj):  # circuit_obj = {"guard": (IP, [ports])}
        
        #circuit_json = json.loads(circuit_obj)
        circuit_json = circuit_obj

        self.__CIRCUIT = {
            (circuit_json["guard"][0], circuit_json["guard"][1]) : [None, None], # No key yet 
            (circuit_json["middle"][0], circuit_json["middle"][1]) : [None, None],
            (circuit_json["exit"][0], circuit_json["exit"][1]) : [None, None]
        }


    # Packets maker
    def TOR_packet_maker(self, code, address, msg=None):
        
        try:

            match code:

                case 0:    # Diffie Hellman code
                    self.__key_exchange_maker(address)

                case _:    # Send to App Server
                    self.__app_server_message_maker(msg, address)

            # Make Onion - Encrypt 3 layers
            if list(self.__CIRCUIT.values())[0][1]:  # if one shared key
                for addr, node_info in reversed(self.__CIRCUIT.items()):
                    
                    if node_info[1] == None:
                        continue

                    msg_str = json.dumps(self.__TOR_msg)

                    cipher, nonce = Crypto_Utils.Crypto_Utils.encrypt_data(node_info[1], msg_str.encode("utf8"))
                    self.__TOR_msg["payload"] = base64.b64encode(cipher).decode('utf-8')
                    self.__TOR_msg["nonce"] = base64.b64encode(nonce).decode('utf-8')
                    self.__TOR_msg["successor_IP"] = addr[0]
                    self.__TOR_msg["open_port"] = addr[1]
                    self.__TOR_msg["code"] = PROCESS_AND_FORWARD_CODE 

        except Exception as e:
            print(e)
            raise Exception("The data is not valid.")

        return self.__TOR_msg

        
    def __key_exchange_maker(self, address):
        
        self.__CIRCUIT[address][0], public_key = Crypto_Utils.Crypto_Utils.generate_public_key()
        
        self.__TOR_msg = {
            "successor_IP" : address[0],
            "open_port" : address[1],
            "payload" : base64.b64encode(public_key).decode('utf-8'),
            "code" : DIFFIE_HELLMAN_CODE
        }
        

    def __app_server_message_maker(self, msg, address):

            self.__TOR_msg = {
                "successor_IP" : address[0], # SERVER STATIC ADDRESS
                "open_port" : address[1],
                "payload" : msg
            }


    # Responses handler
    def TOR_response_handler(self, response):  # circuit object
        
        try:
            
            response_json = json.loads(response)
            
            if list(self.__CIRCUIT.values())[0][1]:  # if one shared key
                for node_info in self.__CIRCUIT.values():
                    
                    if node_info[1] == None:
                        break

                    response_str = Crypto_Utils.Crypto_Utils.decrypt_data(node_info[1], base64.b64decode(response_json["payload"]),
                     base64.b64decode(response_json["nonce"]))
                    response_json = json.loads(response_str)

            if "code" in response_json and response_json ["code"] == DIFFIE_HELLMAN_CODE:

                self.__key_exchange_handler(base64.b64decode(response_json["payload"]))

            else: # Recieved from App Server

                    self.__app_logic_unit.response_handler(response_json)       

        except:
            raise Exception("The packet is not valid (Protocol does not match).")

        
    def __key_exchange_handler(self, public_key):
        
        addr = self.__find_first_node_without_key()
        self.__CIRCUIT[addr][1] = Crypto_Utils.Crypto_Utils.derive_key(self.__CIRCUIT[addr][0], public_key)  # assign shared key
         

    def __app_server_message_handler(self, msg):
        
        sys.stdout.write(json.dumps(msg))
        sys.stdout.write('\n[Me] '); sys.stdout.flush()    


    # Helping methods
    def __find_first_node_without_key(self):
         for addr in self.__CIRCUIT.keys():
                if self.__CIRCUIT[addr][1] == None:
                    return addr


        
 