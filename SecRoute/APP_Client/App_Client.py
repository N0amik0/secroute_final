# chat_client.py

import sys
import socket
import select
import json
import TOR_Logic_Unit
import Client_Logic_Unit

#NODE_PORT = 9001
#NODE_PORT2 = 9012
#NODE_PORT3 = 9033

#NODES_ADDR = [("127.0.0.1", NODE_PORT) , ("127.0.0.1", NODE_PORT2), ("127.0.0.1", NODE_PORT3)]
 # build circuit = {"guard": (IP, [ports])}
SERVER_ADDRESS = ("127.0.0.1", 9009)
TOR_DIR_ADDR = ('127.0.0.1', 8283)
RECV_BUFFER = 4096 

TOR_LOGIC_UNIT = TOR_Logic_Unit.TOR_Logic_Unit()
CLIENT_LOGIC_UNIT = Client_Logic_Unit.Client_Logic_Unit()

CIRCUIT = None


def calculate_circuit():
    global CIRCUIT

    try:
        TOR_Dir_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        TOR_Dir_sock.connect(TOR_DIR_ADDR) 
        nodes_data = TOR_Dir_sock.recv(RECV_BUFFER * 4).decode()

        nodes_data = nodes_data.replace("true", "True")
        nodes_data = nodes_data.replace("false", "False")
        nodes_data = nodes_data.replace("ip", "IP")

        nodes_data = list(eval(nodes_data))
        CIRCUIT = TOR_Logic_Unit.TOR_Logic_Unit.calculate_circuit(nodes_data)
        TOR_LOGIC_UNIT.load_circuit(CIRCUIT)

    except Exception as e:
        print("Circuit could not be built. Your connection is not safe!")
        sys.exit()


def exchange_keys(guard_sock):
    print ('Connected to remote host. Starting key exchange via Diffie - Hellman...')

    for addr in CIRCUIT.values():
       
        key_exchange_request = TOR_LOGIC_UNIT.TOR_packet_maker(0, address=addr)
        guard_sock.send(json.dumps(key_exchange_request).encode())
        data = guard_sock.recv(4096).decode()
        
        if not data:
            print ('\nDisconnected from chat server')
            sys.exit()
        else :
            TOR_LOGIC_UNIT.TOR_response_handler(data)


def start_connection():
    
    calculate_circuit()
    guard_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

    # connect to guard node
    try :
        guard_sock.connect(CIRCUIT["guard"])
    except :
        print ('Unable to connect to guard node.')
        sys.exit()

    exchange_keys(guard_sock=guard_sock)

    print("""You can start send messages
            Action menu:
            Send Message - 200, ReceiverID, message content (For Example "200, #0420, Hi")
            Register - 300, email, username, password (For Example "300, samiAuSh@gmail.com, Sami, SaSh69")
            Login - 400, email, password (For Example "400, samiAuSh@gmail.com SaSh69")
            Open New Chat - 600, UserID (For example "600, #0420")
            Logout - 900
        """)
    

    return guard_sock


# Main client function
def chat_client():
    
    s = start_connection()
     
    while True:
        socket_list = [sys.stdin, s]
         
        # Get the list sockets which are readable
        read_sockets, write_sockets, error_sockets = select.select(socket_list , [], [])
         
        for sock in read_sockets:            
            if sock == s:
                # incoming message from remote server, s
                data = sock.recv(RECV_BUFFER).decode()
                if not data:
                    print("Connection was terminated. Calculating new circuit...")
                    socket_list.remove(s)

                    s = start_connection()
                    socket_list.append(s)

                else :
                    #print data process
                    TOR_LOGIC_UNIT.TOR_response_handler(data)
            
            else:
                # user entered a message
                msg = sys.stdin.readline()
                msg = CLIENT_LOGIC_UNIT.request_maker(msg)
                request = TOR_LOGIC_UNIT.TOR_packet_maker(2, SERVER_ADDRESS, msg)
                
                request_str = json.dumps(request)
                s.send(request_str.encode())


if __name__ == "__main__":

    sys.exit(chat_client())

