import json
import base64
import sys

# Relevant Protocol codes
SEND_MSG_CODE = 200
REGISTER_CODE = 300
LOGIN_CODE = 400
OPEN_NEW_CHAT_CODE = 600

BASE_ID = '#0000'
SERVER_ID = '#1111'

class Client_Logic_Unit:
    """
        The Client Logic Unit is responsible to perform all the operations that are connected
        to the application communication and logic.
        Creating requests in the app's protocol, handling responses, etc.

        The class is built in the singleton design pattern.
    """

    __instance = None
    __client_req = ""
    __user_id = "#0000"
    __username = ""

    def __new__(cls):
        if cls.__instance == None:
            cls.__instance = super(Client_Logic_Unit, cls).__new__(cls)

        return cls.__instance

    """
    The method will create a request in the application's communication protocol
    :param msg: the content of the message itself
    :type msg: str
    :return: a message in the communication protocol as a json string
    :rtype: str
    """
    def request_maker(self, msg):
        try:
            msg = msg.strip()  # removing extra "\n"
            request_parts = msg.split(", ")
            match int(request_parts[0]):
                case 200:  # sending a message
                    self.__client_req = json.dumps({'Code': request_parts[0], 'SenderID': self.__user_id,
                                                    'ReceiverID': request_parts[1], 'Data': request_parts[2]})
                case 300:  # signing up
                    self.__client_req = json.dumps({'Code': request_parts[0], 'SenderID': "#0000",
                                                    'ReceiverID': "#1111", 'Data': [request_parts[1], request_parts[2], request_parts[3]]}) 
                                                    # creating a json string with the user details as a list when converted back to json
                case 400:  # logging in
                    self.__client_req = json.dumps({'Code': request_parts[0], 'SenderID': "#0000",
                                                    'ReceiverID': "#1111", 'Data': [request_parts[1], request_parts[2]]})
                case 600:  # creating a new chat
                    self.__client_req = json.dumps({'Code': request_parts[0], 'SenderID': self.__user_id,
                                                    'ReceiverID': "#1111", 'Data': [self.__user_id, request_parts[1]]})
                case 900:  # logging out of the app
                    self.__client_req = json.dumps({'Code': request_parts[0], 'SenderID': self.__user_id,
                                                    'ReceiverID': "#1111"})
                case _:
                    raise Exception("Invalid code")

        except Exception as e:
            print(e)
        return self.__client_req

    """
    The method will handle all responses coming from the server and will update the client's state accordingly
    :param response: a json string with the response from the server
    :type response: str
    :return: none
    """
    def response_handler(self, response):
        try:
            
            match int(response["Code"]):
                case 111:  # an error accured
                    sys.stdout.write("[SecRoute] " + response['Data'] + '\n'); sys.stdout.flush()
                case 200:
                    sys.stdout.write('[' + response["SenderID"] + '] ' + response["Data"] + '\n'); sys.stdout.flush()
                case 250:  # a message was approved
                    sys.stdout.write("[SecRoute] Your message was sent!\n"); sys.stdout.flush()
                case 350:  # successful registration
                    self.__user_id = response['ID']
                    self.__username = response['Username']
                    print("[SecRoute] You successfuly registered to SecRoute!\nYou can now start sending messages\n")
                    #  sys.stdout.write('\n[Me] '); sys.stdout.flush()  
                case 450:  # successful login
                    self.__user_id = response['ID']
                    self.__username = response['Username']
                    print("[SecRoute] You successfuly logged in to SecRoute!\nYou can now start sending messages\n")
                case 555:  # notifications alert
                    notifs = json.loads(response['Notifications'])
                    if not notifs:  # if there are no notifications
                        print("[SecRoute] You have no notifications!\n")
                    else:
                        print("[SecRoute]\n")
                        for key in notifs:
                            print("You have " + str(notifs[key]) + " notification(s) from conversation " + key + "\n")
                case 650:  # succesfull chat opened
                    print("[SecRoute] Conversation " + response['Conversation Name'] + " was added\n")
                case 888:  # broadcast
                    print('[SecRoute] ' + response["Data"] + '\n')
                case 950:
                    self.__user_id = "#0000"
                    self.__username = ""
                    try:
                        sys.exit()
                    except Exception as e:
                        print("[SecRoute] You are no longer connected to SecRoute. Goodbye!\n")
                case _:
                    raise Exception("[SecRoute] Invalid code")

        except Exception as e:
            print(e) 

