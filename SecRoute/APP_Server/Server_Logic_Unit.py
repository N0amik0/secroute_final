import json
import base64
import sys
import random
import Server_DB_Wrapper
import time

# Relevant Protocol codes
SEND_MSG_CODE = 200
REGISTER_CODE = 300
LOGIN_CODE = 400
OPEN_NEW_CHAT_CODE = 600

SERVER_ID = '#1111'
BASE_ID = '#0000'

class Server_Logic_Unit:
    """
        The Server Logic Unit is responsible to perform all the operations that are connected
        to the application communication and logic.
        Creating responses in the app's protocol, handling requests, etc.

        The class is built in the singleton design pattern.
    """

    __instance = None
    __server_resp = ""
    __connecetions_dict = {}  # the dictionary will have users' ID's as keys and their repective sockets as values
    __DB_Wrapper = Server_DB_Wrapper.Server_DB_Wrapper()

    def __new__(cls):
        if cls.__instance == None:
            cls.__instance = super(Server_Logic_Unit, cls).__new__(cls)

        return cls.__instance

    """
    The method will handle a request in the application's communication protocol
    :param msg: the content of the request
    :type msg: str
    :return: none
    """
    def request_handler(self, SOCKET_LIST, curr_socket, server_socket, req):
        try:
            req_j = json.loads(req)
            if curr_socket not in self.__connecetions_dict.values():  # checking if the user in the connection already logged in to the app
                if int(req_j["Code"]) != 300 and int(req_j["Code"]) != 400:
                    resp = "111, #0000, You must login / register to SecRoute!\n"
                    self.response_maker(resp, curr_socket)  # alerting the client that it must login
                    raise Exception("Someone tried to send requests before logging in!")
            if curr_socket in self.__connecetions_dict.values() and (int(req_j["Code"]) == 300 or int(req_j["Code"]) == 400):
                    user_id = list(filter(lambda x: self.__connecetions_dict[x] == curr_socket, self.__connecetions_dict))[0]
                    resp = "111, " + user_id + ", You are already connected to SecRoute!\n"
                    self.response_maker(resp, curr_socket)  # alerting the client that it must ,login
                    raise Exception("Someone tried to connect twice!")

            match int(req_j["Code"]):
                case 200:  # a message was sent
                    is_sent = 0  # the destination isn't connected as default
                    if req_j["ReceiverID"] == req_j["SenderID"]:
                        resp = "111, " + req_j["SenderID"] + ", You can't message yourself!\n"
                        self.response_maker(resp, curr_socket)  # alerting the client
                        raise Exception("Someone tried to send a message to themselves!")
                    convo_name = self.__DB_Wrapper.get_conversation_name(req_j["ReceiverID"], req_j["SenderID"])  # getting the conversation's name
                    if convo_name is None:  # if there's no conversation between the users in the DB
                        resp = "111, " + req_j["SenderID"] + ", The conversation doesn't exist!\n"
                        self.response_maker(resp, curr_socket)  # alerting the client
                        raise Exception("Someone tried to sent a message to a non existing conversation!")
                    if req_j["ReceiverID"] in self.__connecetions_dict.keys():  # checking if the receiver is connected
                        is_sent = 1
                    if self.__DB_Wrapper.add_message(req_j["SenderID"], convo_name, req_j["Data"], is_sent) == False: #  adding message to the database, and checking if it failed
                        resp = "111, " + req_j["SenderID"] + ", Couldn't send message!\n"  # the message wasn't added to the DB
                        self.response_maker(resp, curr_socket)  # alerting the client
                        raise Exception("A message couldn't be sent!")
                    resp = "250, " + req_j["SenderID"]  # approving the message
                    self.response_maker(resp, curr_socket)
                    if is_sent == 1:
                        self.__connecetions_dict[req_j["ReceiverID"]].send(req.encode())  # passing the message
                case 300:  # a client tried to register
                    if self.__DB_Wrapper.is_email_valid(req_j['Data'][0]):
                        user_id = self.__create_id()  # creating a new id
                        if self.__DB_Wrapper.add_user(user_id, req_j['Data'][0], req_j['Data'][1], req_j['Data'][2]):
                            self.__connecetions_dict[user_id] = curr_socket  # adding a new connection
                            resp = "350, " + user_id + ", " + str(req_j['Data'][1])
                            print(req_j['Data'][1] + user_id + " registered!")
                            self.response_maker(resp, curr_socket)
                            time.sleep(0.42)
                            self.__send_connected_users(SOCKET_LIST, server_socket)  # boradcasting the connected users
                        else: # send sign up error response with response handler
                            self.response_maker("111, #0000, Couldn't add a new user - try again", curr_socket)
                    else: # send email error response with response handler
                        self.response_maker("111, #0000, This Email is already used!", curr_socket)
                case 400:  # a client tried to log in
                    if self.__DB_Wrapper.check_password_matches_email(req_j['Data'][0], req_j['Data'][1]):
                        user_details = self.__DB_Wrapper.get_user_details(req_j['Data'][0], req_j['Data'][1])
                        if user_details[0] in self.__connecetions_dict.keys():
                            self.response_maker("111, #0000, This user is already connected!", curr_socket)
                        elif user_details[0] != "#0000":
                            self.__connecetions_dict[user_details[0]] = curr_socket  # adding a new connection
                            resp = "450, " + user_details[0] + ", " + user_details[2]
                            print(user_details[2] + user_details[0] + " logged in!")
                            self.response_maker(resp, curr_socket)
                            time.sleep(0.99)
                            self.__send_notifications(curr_socket, user_details[0])
                            time.sleep(0.99)
                            self.__send_connected_users(SOCKET_LIST, server_socket)  # broadcasting the connected users
                        else: # send login error
                            self.response_maker("111, #0000, Incorrect user information! Please try again", curr_socket)
                    else: # send login error response with response handler
                            self.response_maker("111, #0000, Password and Email don't match!", curr_socket)
                case 600:  # creating a new chat
                    if self.__DB_Wrapper.is_id_valid(req_j['Data'][1]):  # if the receiver id doesn't exist
                        resp = "111, " + req_j['SenderID'] + ", The Receiver's ID doesn't exist - try again"
                        self.response_maker(resp, curr_socket)
                        raise Exception("Someone tried to open a conversation with a nonexistent user!")
                    elif self.__DB_Wrapper.get_conversation_name(req_j['Data'][0], req_j['Data'][1]) is None:
                        if self.__DB_Wrapper.add_conversation(req_j['Data'][0], req_j['Data'][1]):
                            resp = "650, " + req_j['SenderID'] + ", " + str(req_j['Data'][0] + '_' + req_j['Data'][1])
                            print("conversation " + str(req_j['Data'][0] + '_' + req_j['Data'][1]) + " now exists")
                            self.response_maker(resp, curr_socket)
                        else:
                            resp = "111, " + req_j['SenderID'] + ", Couldn't add conversation - try again"
                            self.response_maker(resp, curr_socket)  
                    else:
                        resp = "111, " + req_j['SenderID'] + ", This conversation already exists"
                        self.response_maker(resp, curr_socket)
                case 900:  # a client wants to log out
                    if req_j['SenderID'] != "#0000":
                        try:
                            del self.__connecetions_dict[req_j['SenderID']]  # removing the user from the connections dict
                            resp = "950, " + req_j['SenderID']
                            self.response_maker(resp, curr_socket)
                            SOCKET_LIST.remove(curr_socket)
                            print(req_j['SenderID'] + " disconnected from server")
                            self.__send_connected_users(SOCKET_LIST, server_socket)  # broadcasting the connected users
                        except:
                            resp = "111, " + req_j['SenderID'] + ", This user is not connected"
                            self.response_maker(resp, curr_socket)
                case _:
                    raise Exception("Invalid code")

        except Exception as e:
            print(e)

    """
    The method will create a response to a certain response an will pass it to the correct socket
    :param response_msg: a string with the response's data
    :type response_msg: str
    :param sock: a conversation socket to send the response to
    :type sock: socket
    :return: none
    """
    def response_maker(self, response_msg, sock):
        try:
            response_parts = response_msg.split(", ")
            match int(response_parts[0]):
                case 111:  # sending an error response
                    self.__server_resp = json.dumps({'Code': response_parts[0], 'SenderID': "#1111",
                                                    'RecieverID': response_parts[1], 'Data': response_parts[2]})
                case 250:  # approving that a message was sent
                    self.__server_resp = json.dumps({'Code': response_parts[0], 'SenderID': "#1111",
                                                    'RecieverID': response_parts[1]})
                case 350:  # approving a successful sign up
                    self.__server_resp = json.dumps({'Code': response_parts[0], 'SenderID': "#1111",
                                                    'ReceiverID': response_parts[1], 'ID': response_parts[1], 'Username': response_parts[2]})
                case 450:  # approving a successful login
                    self.__server_resp = json.dumps({'Code': response_parts[0], 'SenderID': "#1111",
                                                    'ReceiverID': response_parts[1], 'ID': response_parts[1], 'Username': response_parts[2]})
                case 555:  # sending notifications to a user
                    self.__server_resp = json.dumps({'Code': response_parts[0], 'SenderID': "#1111",
                                                    'ReceiverID': response_parts[1], 'Notifications': response_parts[2]})
                case 650:  # approving the creation a new chat
                    self.__server_resp = json.dumps({'Code': response_parts[0], 'SenderID': "#1111",
                                                    'RecieverID': response_parts[1], 'Conversation Name': response_parts[2]})
                case 950:  # approving the exit of a user
                    self.__server_resp = json.dumps({'Code': response_parts[0], 'SenderID': "#1111",
                                                    'RecieverID': response_parts[1]})
            sock.send(self.__server_resp.encode())  # sending the response

        except Exception as e:
            print(e)
            self.__server_resp = json.dumps({'Code': 111, 'Data': e})
            sock.send(self.__server_resp.encode())  # sending the response


    # broadcast chat messages to all connected clients
    @staticmethod
    def broadcast (SOCKET_LIST, server_socket, message):
        for socket in SOCKET_LIST:
            # send the message only to peer
            if socket != server_socket: #and socket != sock :
                try :
                    response  = {
                        "Code" : 888,
                        "SenderID": "#1111",
                        "Data" : message
                    }

                    response_str = json.dumps(response)

                    socket.send(response_str.encode())
                except :
                    # broken socket connection
                    socket.close()
                    # broken socket, remove it
                    if socket in SOCKET_LIST:
                        SOCKET_LIST.remove(socket)

    """
    The method will create a special id to a registering client
    return: the new id of the user
    rtype: str
    """
    def __create_id(self):
        val = random.randint(1, 9999)
        while val == 1111: # if the ID is the server's ID
            val = random.randint(1, 9999)
        user_id = str(val).zfill(4)
        user_id = '#' + user_id
        while not self.__DB_Wrapper.is_id_valid(user_id):  # checking if the ID is already used
            val = random.randint(1, 9999)
            while val == 1111:
                val = random.randint(1, 9999)
            user_id = str(val).zfill(4)
            user_id = '#' + user_id
        return user_id
    
    """
    The method will broadcast all of the connected users
    return: None
    """
    def __send_connected_users(self, SOCKET_LIST, server_socket):
        connected_users = []
        user = ""
        for key in self.__connecetions_dict.keys():
            details = self.__DB_Wrapper.get_user_details_by_ID(key)
            user = details[2] + details[0]  # combining the username and ID
            connected_users.append(user)

        msg = "The connected users are: [" + ', '.join(connected_users) + ']'
        self.broadcast(self.__connecetions_dict.values(), server_socket, msg)

    """
    The function will notify the user about the chats that he received messages from when he wasn't connected
    :param sock: the user's connection socket
    :type sock: socket
    :param req_j: the user's ID
    :type req_j: str
    return: None
    """
    def __send_notifications(self, sock, user_id):
        conversations_names = self.__DB_Wrapper.get_user_conversations(user_id)
        notif_dict = {}
        if len(conversations_names) == 0:
            resp = "555, " + user_id + ", {" + "}"  # sending an empty dict
            self.response_maker(resp, sock)
        else:
            for conversation in conversations_names:
                num_of_notifs = self.__DB_Wrapper.get_notifications_from_convo(user_id, conversation)
                if num_of_notifs > 0:
                    notif_dict[conversation] = num_of_notifs
            notif_str = json.dumps(notif_dict)
            resp = "555, " + user_id + ", " + notif_str
            self.response_maker(resp, sock)

