# chat_server.py
 
import sys
import socket
import select
import json
import time
import Server_Logic_Unit

HOST = '0.0.0.0' 
SOCKET_LIST = []
RECV_BUFFER = 4096 
PORT = 9009

SERVER_LOGIC_UNIT = Server_Logic_Unit.Server_Logic_Unit()

def chat_server():

    server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
    server_socket.bind((HOST, PORT))
    server_socket.listen(10)
 
    # add server socket object to the list of readable connections
    SOCKET_LIST.append(server_socket)
 
    print("Chat server started on port " + str(PORT))
 
    while 1:

        # get the list sockets which are ready to be read through select
        # 4th arg, time_out  = 0 : poll and never block
        ready_to_read,ready_to_write,in_error = select.select(SOCKET_LIST,[],[],0)
      
        for sock in ready_to_read:
            # a new connection request recieved
            if sock == server_socket: 
                sockfd, addr = server_socket.accept()
                SOCKET_LIST.append(sockfd)
                print("Client (%s, %s) connected" % addr)
                time.sleep(0.42)
                #  notification = str("[%s:%s] entered our chatting room\n" % addr)
                #  SERVER_LOGIC_UNIT.broadcast(SOCKET_LIST, server_socket, notification)
             
            # a message from a client, not a new connection
            else:
                # process data received from client, 
                try:
                    # receiving data from the socket.
                    data = sock.recv(RECV_BUFFER).decode()
                    # process data by protocol
                    if data:
                        # there is something in the socket
                        time.sleep(0.42)
                        SERVER_LOGIC_UNIT.request_handler(SOCKET_LIST, sock, server_socket, data)
                        time.sleep(0.42) # alternative solution - check for two jsons in node and app
                    else:
                        # remove the socket that's broken    
                        if sock in SOCKET_LIST:
                            SOCKET_LIST.remove(sock)

                        # at this stage, no data means probably the connection has been broken
                        SERVER_LOGIC_UNIT.broadcast(SOCKET_LIST, server_socket, "Client (%s, %s) is offline\n" % addr)
                        print("Client (%s, %s) disconnected" % addr)

                # exception 
                except:
                    SERVER_LOGIC_UNIT.broadcast(SOCKET_LIST, server_socket, "Client (%s, %s) is offline\n" % addr)
                    print("Client (%s, %s) disconnected" % addr)
                    continue

    server_socket.close()
 
if __name__ == "__main__":

    sys.exit(chat_server())


         
