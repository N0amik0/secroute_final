import sqlite3
from datetime import datetime
import json

DB_NAME = "ServerDB.db"

class Server_DB_Wrapper:
    """
    The method will create a connection with the database and will create the tables of necessary
    con is a connection object (from sqlite3 module) that connects the program to the db
    cur is a cursor object (BLOB type, from sqlite3 module) that allows to execute various sqlite3 methods on the database (apply queries and more)
    """
    def __init__(self):
        self.con = sqlite3.connect(DB_NAME)
        self.cur = self.con.cursor()
        self.cur.execute("""CREATE TABLE IF NOT EXISTS CONVERSATIONS (CONVERSATION_ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
                            CONVERSATION_NAME TEXT NOT NULL);""")  # creating the conversations table
        self.cur.execute("""CREATE TABLE IF NOT EXISTS MESSAGES (MSG_ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
                            SENDER TEXT NOT NULL, CONVERSATION_NAME INTEGER NOT NULL, 
                            SENT_DATETIME TEXT NOT NULL, MSG_TEXT TEXT NOT NULL, MSG_SENT INTEGER DEFAULT 0,
                            FOREIGN KEY(CONVERSATION_NAME) REFERENCES CONVERSATIONS(CONVERSATION_NAME));""")  # creating the messages table
        self.cur.execute("""CREATE TABLE IF NOT EXISTS USERS (USER_ID TEXT NOT NULL PRIMARY KEY, 
                            EMAIL TEXT NOT NULL, USERNAME TEXT NOT NULL,  PASSWORD TEXT NOT NULL);""")  # creating the users table

    """
    The method destroys the object and it's contents
    """
    def __del__(self):
        self.con.close()  # closing the connection with the db
    
    """
    The method will add a new conversation to the app server's db
    :param first_id: an id of one of the users
    :type first_id: str
    :param second_id: an id of one of the users
    :type second_id: str
    :return: if the conversation was added or not
    :rtype: bool
    """
    def add_conversation(self, first_id, second_id):
        convo_name = first_id + '_' + second_id
        try:
            query = f"INSERT INTO CONVERSATIONS (CONVERSATION_NAME) VALUES ('{convo_name}')"
            self.cur.execute(query)  # Adding the new conversation
            self.con.commit()
            return True
        except sqlite3.Error as e:
            print("Failed to add conversation", e)
        return False

    """
    The method will add a new conversation to the app server's db
    :param user_id: the id of the user
    :type user_id: str
    :param email: the email of the user
    :type email: str
    :param username: the user's name
    :type username: str
    :param password: the user's password
    :type password: str
    :return: if the user was added or not
    :rtype: bool
    """
    def add_user(self, user_id, email, username, password):
        data = [user_id, email, username, password]
        try:
            self.cur.execute("INSERT INTO USERS (USER_ID, EMAIL, USERNAME, PASSWORD) VALUES (?, ?, ?, ?)",
                             data)  # Adding the new user
            self.con.commit()
            return True
        except sqlite3.Error as e:
            print("Failed to add user", e)
        return False

    """
    The method will add a new message to the app server's db
    :return: if the message was added or not
    :rtype: bool
    """
    def add_message(self, sender_id, convo_name, data, is_sent):
        now = datetime.now()  # getting the current time
        dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
        data = [sender_id, convo_name, dt_string, data, is_sent]
        try:
            self.cur.execute("INSERT INTO MESSAGES (SENDER, CONVERSATION_NAME, SENT_DATETIME, MSG_TEXT, MSG_SENT) VALUES (?, ?, ?, ?, ?)",
                             data)  # Adding the new message
            self.con.commit()
            return True
        except sqlite3.Error as e:
            print("Failed to create message", e)
        return False

    """
    The method will check if a given id already exists in the db
    :param potential_id: a randomized id
    :type potential_id: str
    :return: True if the id doesn't exist in the db / False if the id exists in the db
    :rtype: bool
    """
    def is_id_valid(self, potential_id):
        query = f"SELECT * FROM USERS WHERE USER_ID LIKE '{potential_id}'"
        try:
            result = (self.cur.execute(query)).fetchall()  # checking for the id
            if len(result) == 0:
                return True
        except sqlite3.Error as e:
            print(e)
        return False

    """
    The method will check if a given email already exists in the db
    :param potential_email: an email
    :type potential_email: str
    :return: True if the email doesn't exist in the db / False if the email exists in the db
    :rtype: bool
    """
    def is_email_valid(self, potential_email):
        query = f"SELECT * FROM USERS WHERE EMAIL GLOB '{potential_email}'"
        try:
            result = (self.cur.execute(query)).fetchall()  # checking for the id
            if len(result) == 0:
                return True
        except sqlite3.Error as e:
            print(e)
        return False

    """
    The method will check if a given password corresponds with a given email
    :param email: an email
    :type email: str
    :param password: an password
    :type password: str
    :return: True if the password corresponds witht ehe email / False if it doesn't
    :rtype: bool
    """
    def check_password_matches_email(self, email, password):
        query = f"SELECT * FROM USERS WHERE EMAIL GLOB '{email}' AND PASSWORD GLOB '{password}'"
        try:
            result = (self.cur.execute(query)).fetchall()  # checking for the user
            if len(result) == 0:
                return False
        except sqlite3.Error as e:
            print(e)
        return True

    """
    The method will get a user's email and password
    :param email: an email
    :type email: str
    :param password: an password
    :type password: str
    :return: The user's details as a list if it exists / A list that contains '#0000' if it doesm't
    :rtype: list
    """
    def get_user_details(self, email, password):
        query = f"SELECT * FROM USERS WHERE EMAIL GLOB '{email}' AND PASSWORD GLOB '{password}'"
        try:
            result = (self.cur.execute(query)).fetchall()  # checking for the user
            if len(result) == 1:
                return list(result[0])
        except sqlite3.Error as e:
            print(e)
        return ['#0000']
    
    """
    The method will get a user's ID
    :param ID: an ID
    :type email: st
    :param password: an password
    :type password: str
    :return: The user's details as a list if it exists / A list that contains '#0000' if it doesm't
    :rtype: list
    """
    def get_user_details_by_ID(self, ID):
        query = f"SELECT * FROM USERS WHERE USER_ID GLOB '{ID}'"
        try:
            result = (self.cur.execute(query)).fetchall()  # checking for the user
            if len(result) == 1:
                return list(result[0])
        except sqlite3.Error as e:
            print(e)
        return ['#0000']

    """
    The method will return the conversation name of 2 users in the database
    :param first_id: an id of one of the users
    :type first_id: str
    :param second_id: an id of one of the users
    :type second_id: str
    :return: The conversation's name if it conversation exists / Null if the conversation doesn't exist
    :rtype: str / None
    """
    def get_conversation_name(self, first_id, second_id):
        query = f"SELECT * FROM CONVERSATIONS WHERE CONVERSATION_NAME LIKE '%{first_id}%' and CONVERSATION_NAME LIKE '%{second_id}%'"
        try:
            result = (self.cur.execute(query)).fetchall()  # checking for the conversation
            if len(result) == 1:
                return result[0][1]  # returning the name
        except sqlite3.Error as e:
            print(e)
        return None

    """
    The method will return all of the conversation names that the user participates in
    :param user_id: the user's id
    :type user_id: str
    :return: a list of the conversations' names
    :rtype: list
    """
    def get_user_conversations(self, user_id):
        query = f"SELECT CONVERSATION_NAME FROM CONVERSATIONS WHERE CONVERSATION_NAME LIKE '%{user_id}%'"
        convos = []
        try:
            names = (self.cur.execute(query)).fetchall()  # getting all the conversations' names
            for name in names:
                convos.append(name[0])
        except sqlite3.Error as e:
            print(e)
        return convos

    """
    The method will check for all of the notifications of a certain user and will return
    a dictionary with the amount of notifications from each user
    :param user_id: the id of a certain user
    :type user_id: str
    :return: the number of notifications a user has from a given convo
    :rtype: int
    """
    def get_notifications_from_convo(self, user_id, conversation_name):
        query = f"SELECT DISTINCT COUNT(*) FROM MESSAGES WHERE CONVERSATION_NAME LIKE '{conversation_name}' AND MSG_SENT = 0 AND SENDER NOT LIKE '{user_id}'"
        num_of_notifications = 0
        try:
            result = (self.cur.execute(query)).fetchall()  # getting amount of notifications
            num_of_notifications = result[0][0]
        except sqlite3.Error as e:
            print(e)
        return num_of_notifications