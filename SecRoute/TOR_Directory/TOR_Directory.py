# TOR Directory python script
 
import sys
import socket
import select
import TOR_Dir_Logic_Unit

IP = "0.0.0.0"
OPEN_PORTS = [8182, 8283, 8384]  # 0 - registration service port, 1 - network info service, 2 - nodes communication   

SOCKET_LIST = []
RECV_BUFFER = 4096 

LOGIC_UNIT = TOR_Dir_Logic_Unit.TOR_DIR_Logic_Unit()
TRMINATE_CONN_STRINGS = ["The node has been registered successfully!", "Node is not recognized"]

def TOR_DIR():

    for port in OPEN_PORTS:
        listening_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        listening_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        listening_socket.bind((IP, port))
        listening_socket.listen(10)
    
        # add socket object to the list of readable connections
        SOCKET_LIST.append(listening_socket)

    print("Registration service started on port " + str(OPEN_PORTS[0]))
    print("Network information service started on port " + str(OPEN_PORTS[1]))
    print("Nodes communiction service started on port " + str(OPEN_PORTS[2]))
 
    while 1:

        # get the list sockets which are ready to be read through select
        # 4th arg, time_out  = 0 : poll and never block
        ready_to_read, ready_to_write, in_error = select.select(SOCKET_LIST,[],[],0)
       
        for sock in ready_to_read:
            # New connection want a service
            if sock in SOCKET_LIST[:3]:
                
                response = None
                sockfd, addr = sock.accept()
                SOCKET_LIST.append(sockfd)
                print("New connection recieved from (%s, %s)." % addr)
                # time.sleep(0.42)
                
                # registration service port
                if sock == SOCKET_LIST[0]:
                    response = LOGIC_UNIT.TOR_Dir_msg_handler(sockfd, service=0)
                    sockfd.send(response.encode()) 
                
                # network info service
                elif sock == SOCKET_LIST[1]:
                    response = LOGIC_UNIT.TOR_Dir_msg_handler(service=1) 
                    sockfd.send(response.encode()) 
                    sockfd.close()
                    SOCKET_LIST.remove(sockfd)
                    print("(%s, %s) disconnected" % addr)

                # nodes communication 
                else:
                    msg = sockfd.recv(RECV_BUFFER).decode()
                    recieved_addr = tuple(msg.split(","))

                    if recieved_addr[0] != addr[0]:
                        sockfd.close()
                        SOCKET_LIST.remove(sockfd)
                    else:
                        response = LOGIC_UNIT.TOR_Dir_msg_handler(service=2, msg=msg, sock=sockfd, addr=recieved_addr)

                    # Node is not recognized
                    if response in TRMINATE_CONN_STRINGS:
                            sockfd.close()
                            SOCKET_LIST.remove(sockfd)

            # a message from a connected computer, not a new connection
            else:
                # process data recieved from a connected computer, 
                try:

                    data = sock.recv(RECV_BUFFER).decode()
                    if data:
                        data = data.rstrip()
                        
                        if not data: 
                            continue
                        
                        # there is something in the socket
                        response = LOGIC_UNIT.TOR_Dir_msg_handler(sock, msg=data)
                        sock.send(response.encode())

                        if response in TRMINATE_CONN_STRINGS:
                            sock.close()
                            SOCKET_LIST.remove(sock)

                    else:
                        # remove the socket that's broken    
                        if sock in SOCKET_LIST:
                            SOCKET_LIST.remove(sock)

                        # at this stage, no data means probably the connection has been broken
                        print("(%s, %s) disconnected" % addr)
                        response = LOGIC_UNIT.TOR_Dir_msg_handler(service=2, msg=data, sock=sock)
                        if response: print(response)

                # exceptions and errors 
                except:
                    print("(%s, %s) disconnected" % addr)
                    continue

    for sock in SOCKET_LIST[:4]:
        sock.close()


if __name__ == "__main__":

    sys.exit(TOR_DIR())