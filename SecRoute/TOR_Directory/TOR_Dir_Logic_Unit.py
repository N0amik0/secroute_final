import json
import time
import ipaddress
import threading
import TOR_Dir_DB_Wrapper

SLEEP_TIME = 30#60 * 5
LOCK = threading.Lock()

class TOR_DIR_Logic_Unit:
    '''
    The TOR Directory Logic Unit is responsible:
    [1] to provide nodes registration service
    [2] to gather information about the TOR network
    [3] to provide the nodes information to build a circuit 

    The class is built in the singleton design pattern.

    Attributes:
        TODO
    '''
    
    __REGISTRATION_MESSAGES = {
        0 : "Hello, this is registration service from SecRoute TOR Directory! Please enter IP address (format: x.x.x.x): ",
        1 : "Enter open port: ",
        2 : "Enter the node's family addresses (format: x.x.x.x,x.x.x.x,...), in case of no family enter x: ",
        3 : "Enter 1 if the node is GUARD else enter 0: ",
        4 : "The node has been registered successfully!"
    }

    __CONNECTED_NODES = {} # sock : (IP, port)
    __REGISTRATION_DICT = {} # sock : [state (int), [IP, port, family, isGuard, isAvailable]]

    __response = None
    __instance = None
    __DB_Wrapper = TOR_Dir_DB_Wrapper.Tor_Dir_DB_Wrapper()

    def __new__(cls):
        
        if (cls.__instance == None):
            cls.__instance = super(TOR_DIR_Logic_Unit, cls).__new__(cls)

        #cls.__updating_thread = threading.Thread(target=cls.__instance.__update_nodes_info, daemon=True)
        #cls.__updating_thread.start()

        return cls.__instance


    # Messages handler. service - 0: registraion, 1: network info, 2: nodes communication
    def TOR_Dir_msg_handler(self, sock=None, service=None , msg=None, addr=None):
        
        if not service and sock in self.__REGISTRATION_DICT.keys():
            service = 0
        
        elif not service and sock in self.__CONNECTED_NODES.keys():
            service = 2


        self.__response = None

        try:

            match service:

                case 0:    # Registraion service
                    self.__registration_handler(sock, msg)

                case 1:    # Request for network info - available nodes
                    self.__network_info_handler()

                case 2:
                    self.__nodes_communication_handler(sock, addr)


        except Exception as e:
            print(e)
            #raise Exception("The data is not valid.")

        return self.__response

        
    def __registration_handler(self, sock, msg):
        
        # First request for registration
        if sock not in self.__REGISTRATION_DICT.keys():
            
            self.__REGISTRATION_DICT[sock] = [0, []]  # state 0
            self.__response = self.__REGISTRATION_MESSAGES[0]
        
        try:
            if msg:
                data = msg
                # check for error
                match self.__REGISTRATION_DICT[sock][0]:
                    
                    case 0:  # IP should be recieved
                        if not TOR_DIR_Logic_Unit.__validate_IP(msg):
                            raise Exception("IP address should be in format x.x.x.x and each octet should be between 1-254. Try again: ")
                    
                    case 1:  # port should be recieved
                        if not TOR_DIR_Logic_Unit.__validate_port(msg):
                            raise Exception("Port number should be between 0-65535. Try again: ")
                    
                    case 2:  # family should be recieved
                        
                        if data != 'x':
                            data = msg.split(",")
                            
                            for ip in data:
                                if not TOR_DIR_Logic_Unit.__validate_IP(ip):
                                    raise Exception("Family IP addresses should be in format x.x.x.x,x.x.x.x,... and each octet of IP adress should \
                                        be between 1-254. Try again: ") 
                        else:
                            data = []

                    case 3:  # is guard should be recieved
                        if msg not in ['0', '1']:
                            raise Exception("Please enter 0 or 1 only. Try again: ")

                            #data = int(msg)

                # data is valid
                self.__REGISTRATION_DICT[sock][1].append(data)
                self.__REGISTRATION_DICT[sock][0] += 1 # move to next state
                self.__response = self.__REGISTRATION_MESSAGES[self.__REGISTRATION_DICT[sock][0]]

                if self.__REGISTRATION_DICT[sock][0] == 4:
                    node = self.__REGISTRATION_DICT[sock][1]
                    self.__DB_Wrapper.add_node(node[0], node[1], node[2], node[3], False)

        except Exception as e:
            self.__response = str(e)
        

    def __network_info_handler(self):
        nodes_list = self.__DB_Wrapper.get_available_nodes_data()
        self.__response = ",".join(nodes_list)


    def __nodes_communication_handler(self, sock, addr):
        
        if addr:
            # Establish msg 
            node = self.__DB_Wrapper.get_specific_node(addr)
            if not node:
                self.__response = "Node is not recognized"
                raise Exception("No registered node")

            node["is_available"] = True
            self.__DB_Wrapper.update_node(node)
            
            if sock not in self.__CONNECTED_NODES.keys():
                self.__CONNECTED_NODES[sock] = addr
            
        else:
            if sock in self.__CONNECTED_NODES.keys():
                node = self.__DB_Wrapper.get_specific_node(self.__CONNECTED_NODES[sock])
                node["is_available"] = False
                self.__DB_Wrapper.update_node(node)
                del self.__CONNECTED_NODES[sock]



            
            self.__response = "Node has fallen and updated."



    # The thread function to update network's data
    # def __update_nodes_info(self):
        
    #     up_msg = {
    #         "code" : 908,
    #         "payload" : "are you up?"
    #     }

    #     up_msg = json.dumps(up_msg)
    #     while True:
    #         time.sleep(SLEEP_TIME)
    #         print("start thread!")
    #         LOCK.acquire()
    #         for node_sock, node_addr in self.__CONNECTED_NODES.items():
                
    #             try:
    #                 node_sock.send(up_msg.encode())
    #                 print("send msg")
    #                 up_response = node_sock.recv(1024).decode()
    #                 print(up_response)
    #                 up_response_json = json.loads(up_response)
    #                 print(up_response_json)
                    
    #                 if up_response_json["code"] != 909 or up_response_json["payload"] != "are you up? I am up :)":
    #                     raise Exception("Message do not match the protocol")
    #                 print("node is awake good!")
    #             except Exception as e:
    #                 print(e)
    #                 print("node is not available")
    #                 del self.__CONNECTED_NODES[node_sock]

    #                 node_to_update = self.__DB_Wrapper.get_specific_node(node_addr)
    #                 node_to_update["is_available"] = False
    #                 self.__DB_Wrapper.update_node(node_to_update)
    #                 break
    #         LOCK.release()


    # Helping methods
    @staticmethod
    def __validate_IP(ip_string):
        try:
            ipaddress.ip_address(ip_string)
            return True
        except ValueError:
            return False

    @staticmethod
    def __validate_port(port_string):
        try:
            port = int(port_string)
            return True if (port >= 1 and port <= 65535) else False
        except:
            return False


        
 