import sqlite3
import json

DB_NAME = "TorDirDB.db"


class Tor_Dir_DB_Wrapper:
    """
    The method will create a connection with the database and will create the tables of necessary
    con is a connection object (from sqlite3 module) that connects the program to the db
    cur is a cursor object (BLOB type, from sqlite3 module) that allows to execute various sqlite3 methods on the database (apply queries and more)
    """
    def __init__(self):
        self.con = sqlite3.connect(DB_NAME, check_same_thread=False)
        self.cur = self.con.cursor()
        self.cur.execute("""CREATE TABLE IF NOT EXISTS NODES (NODE_ID INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT, 
                            IP TEXT NOT NULL, OPEN_PORT INTEGER NOT NULL, FAMILY TEXT NOT NULL,
                            IS_GUARD INTEGER DEFAULT 0, IS_AVAILABLE INTEGER DEFAULT 0);""")  # creating the table

    """
    The method destroys the object and it's contents
    """
    def __del__(self):
        self.con.close()  # closing the connection with the db

    """
    The method will add a new TOR Node into the tor directory db
    :param ip: The node's IP
    :type ip: str
    :param open_port: The node's open port
    :type open_ports: int
    :param family: The node's family nodes' ips
    :type family: list
    :param is_guard: If the node is a guard node or not
    :type is_guard: bool
    :param is_available: If the node is available to communicate
    :type is_guard: bool
    :return: if the node was added or not
    :rtype: bool
    """
    def add_node(self, ip, open_port, family, is_guard, is_available):
        data = [ip, str(open_port), ':'.join(family), is_guard, is_available]
        try:
            self.cur.execute("INSERT INTO NODES (IP, OPEN_PORT, FAMILY,IS_GUARD, IS_AVAILABLE) VALUES (?, ?, ?, ?, ?)",
                             data)  # Adding the new node
            self.con.commit()
            return True
        except sqlite3.Error as e:
            print("Failed to add node", e)
        return False

    """
    The method will update a given node's data in the database
    :param node_json: a json object with the node's updated information
    :type node_json: json object
    :return: If the table was updated or not
    :rtype: bool
    """
    def update_node(self, node_json):
        try:
            query = f"UPDATE NODES SET IP = '{node_json['ip']}', OPEN_PORT = {node_json['open_port']}, FAMILY = '{':'.join(node_json['family'])}'," \
                    f" IS_GUARD = {str(int(node_json['is_guard']))}, IS_AVAILABLE = {str(int(node_json['is_available']))} WHERE NODE_ID = {node_json['node_id']}"  # Building the update query
            self.cur.execute(query)  # Updating the node
            self.con.commit()
            return True
        except sqlite3.Error as e:
            print("Failed to update sqlite table", e)
        return False

    """
    The method will remove and delete a given node from the database
    :param node_json: a json object with the node's information
    :type node_json: json object
    :return: If the node was deleted(True) or not (False)
    :rtype: bool
    """
    def delete_node(self, node_json):
        try:
            query = f"DELETE FROM NODES WHERE NODE_ID = {node_json['node_id']}"
            self.cur.execute(query)  # Deleting the node
            self.con.commit()
            return True
        except sqlite3.Error as e:
            print("Failed to update sqlite table", e)
        return False

    """
    The method will clear all of the rows from the nodes table in the database
    :return: If the table was cleared(True) or not (False)
    :rtype: bool
    """
    def clear_nodes_table(self):
        try:
            query = f"DELETE FROM NODES"
            self.cur.execute(query)  # Deleting all the nodes
            self.con.commit()
            return True
        except sqlite3.Error as e:
            print("Failed to clear sqlite table", e)
        return False

    """
    The method will return the contents of the TOR Directory database NODES table
    :return nodes_data: A list that contains the data of each node as a json object
    :rtype: list
    """
    def get_nodes_data(self):
        result = self.cur.execute("SELECT * FROM NODES")
        nodes = result.fetchall()
        nodes_data = []
        for node in nodes:
            new_node = {
                'node_id': int(node[0]),
                'ip': node[1],
                'open_port': int(node[2]),
                'family': node[3].split(":"),
                'is_guard': bool(node[4]),
                'is_available': bool(node[5])
            }
            nodes_data.append(json.dumps(new_node))
        return nodes_data


    def get_available_nodes_data(self):
        result = self.cur.execute("SELECT * FROM NODES WHERE IS_AVAILABLE = 1")
        nodes = result.fetchall()
        nodes_data = []
        for node in nodes:
            new_node = {
                'node_id': int(node[0]),
                'ip': node[1],
                'open_port': int(node[2]),
                'family': node[3].split(":"),
                'is_guard': bool(node[4]),
                'is_available': bool(node[5])
            }
            nodes_data.append(json.dumps(new_node))
        return nodes_data


    def get_specific_node(self, address):
        
        result = self.cur.execute(f"SELECT * FROM NODES WHERE IP = '{address[0]}' AND OPEN_PORT = {address[1]}")
        
        try:
            result = result.fetchall()[0]
        except:
            return None

        node = {
            'node_id': int(result[0]),
            'ip': result[1],
            'open_port': int(result[2]),
            'family': result[3].split(":"),
            'is_guard': bool(result[4]),
            'is_available': bool(result[5])
        }

        return node
        
        