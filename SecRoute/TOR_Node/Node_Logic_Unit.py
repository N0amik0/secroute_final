import json
import base64
import Crypto_Utils
import Connection

CLIENT_TO_SERVER = 1
SERVER_TO_CLIENT = 2
PASS_MESSAGE = 1

class Logic_Unit:
    '''
    The TOR Node Logic Unit is responsible to perform the main operation according to the request (data).
    Encrypt, Decrypt Or Key - Exchange via Diffie Hellman. 

    The class is built in the singleton design pattern.

    Attributes:
        __response: The data revieved to process
    '''

    __instance = None
    __response = None

    def __new__(cls):
        
        if (cls.__instance == None):
            cls.__instance = super(Logic_Unit, cls).__new__(cls)

        return cls.__instance


    def process_data(self, connection_obj, data):  # data_json can be pliantext (str) if recieved from server
        try:

            match data["code"]:

                case 0:    # Diffie Hellman code
                    self.__key_exchange_handler(connection_obj, data)

                case 1:    # Forward to another node
                    self.__node_messages_handler(connection_obj, data)

                case 908:    # TOR Directory 
                    self.__TOR_directory_handler(data)
                
                case _:
                    raise Exception("Do not match protocol.")

        except Exception as e:
            
            if connection_obj.direction == CLIENT_TO_SERVER:
               raise Exception("The packet is not valid (Protocol does not match).")

            else:
                self.__node_messages_handler(connection_obj, data)

        return self.__response

        
    def __key_exchange_handler(self, connection_obj, msg):
        
        connection_obj.private_key, public_key = Crypto_Utils.Crypto_Utils.generate_public_key()
        
        self.__response  = {
            "code" : 0,
            "payload" : base64.b64encode(public_key).decode('utf-8')
        }
        
        other_public_key = base64.b64decode(msg["payload"])

        connection_obj.shared_key = Crypto_Utils.Crypto_Utils.derive_key(connection_obj.private_key, other_public_key)


    def __node_messages_handler(self, connection_obj, msg):
        
        # remove one encryption layer from the "onion"
        if connection_obj.direction == CLIENT_TO_SERVER:

            decrypted_msg = Crypto_Utils.Crypto_Utils.decrypt_data(connection_obj.shared_key, base64.b64decode(msg["payload"]), 
            base64.b64decode(msg["nonce"]))
                
            self.__response  = json.loads(decrypted_msg.decode('utf-8'))

            if "code" not in self.__response:
                connection_obj.is_server = True

        # append one encryption layer to the "onion"
        elif connection_obj.direction == SERVER_TO_CLIENT:
            
            encrypted_msg, nonce = Crypto_Utils.Crypto_Utils.encrypt_data(connection_obj.shared_key, msg.encode("utf8"))

            self.__response = {
                "code" : PASS_MESSAGE,
                "nonce" : base64.b64encode(nonce).decode('utf-8'),
                "payload" :  base64.b64encode(encrypted_msg).decode('utf-8')
            }


    def __TOR_directory_handler(self, msg):

        self.__response = {
            "code" : 909,
            "payload" : (msg["payload"] + " I am up :)")
        }
        

        

 