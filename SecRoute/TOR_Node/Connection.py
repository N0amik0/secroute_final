class Connection:
    '''
    The Connection object represenst a spesific connection of a TOR Node.

    Attributes:
        info (json): The message's content
        successor_sock (socket) : The socket to send to
        private_key (bytes) : The key used to encrypt / decrypt this connection
        direction (int) : 1 = client -> server or 2 = server -> clinet
        is_server (bool) : if the connection is with the server
    '''

    def __init__(self, direction=None, sock=None, private_key=None, shared_key=None, is_server=False):
        self.info = None
        self.successor_sock = sock
        self.private_key = private_key
        self.shared_key = shared_key
        self.direction = direction
        self.is_server = is_server
