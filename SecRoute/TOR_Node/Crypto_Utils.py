import json
from base64 import *
from Crypto.Cipher import AES
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives import hashes
from cryptography.hazmat.primitives.asymmetric import ec
from cryptography.hazmat.primitives.kdf.hkdf import HKDF


class Crypto_Utils:

    @staticmethod
    def generate_public_key():
        
        private_key = ec.generate_private_key(ec.SECP384R1())  # generating a private key
        public_key = private_key.public_key()  # generating a safe public key with the private key

        public_key = public_key.public_bytes(encoding=serialization.Encoding.PEM,
                                             format=serialization.PublicFormat.SubjectPublicKeyInfo)  # serializing the public key
        
        return private_key, public_key

    @staticmethod
    def derive_key(private_key, public_key):
        
        public_key = serialization.load_pem_public_key(public_key, )  # de serializing the other public key
        shared_key = private_key.exchange(ec.ECDH(), public_key)  # exchanging the keys
        derived_key = HKDF(algorithm=hashes.SHA256(), length=32, salt=None, info=b'sami', ).derive(
            shared_key)  # getting a key that allows encryption / decryption

        return derived_key

    """
        The method will perform a Diffie-Hellman key exchange with a given socket and will return the shared ciphering key with the other socket
        :param communication_socket: a tcp communication socket
        :type request: socket
        :return: a shared ciphering key with the socket
        :rtype: bytes class object
    """
    @staticmethod
    def dh_KeyExchange(communication_socket):
        private_key = ec.generate_private_key(ec.SECP384R1())  # generating a private key
        public_key = private_key.public_key()  # generating a safe public key with the private key

        public_key = public_key.public_bytes(encoding=serialization.Encoding.PEM,
                                             format=serialization.PublicFormat.SubjectPublicKeyInfo)  # serializing the public key
        communication_socket.sendall(public_key)  # sending the key
        other_public_key = communication_socket.recv(2048)  # getting the peer's public key
        other_public_key = serialization.load_pem_public_key(other_public_key, )  # de serializing the other public key
        shared_key = private_key.exchange(ec.ECDH(), other_public_key)  # exchanging the keys
        derived_key = HKDF(algorithm=hashes.SHA256(), length=32, salt=None, info=b'sami', ).derive(
            shared_key)  # getting a key that allows encryption / decryption
        return derived_key

    """
            The method will perform an AES encryption on given data with a given encryption key. The method will return a cipher with a nonce.
            :param key: an encryption key
            :type request: bytes class 
            :param data: data to encrypt
            :type request: str
            :return: tuple that contains cipher text and nonce
            :rtype: tuple
    """
    @staticmethod
    def encrypt_data(key, data):
        cipher = AES.new(key, AES.MODE_CTR)  # creating cipher object
        cipher_text_bytes = cipher.encrypt(data)  # encrypting the data

        return cipher_text_bytes, cipher.nonce

    """
                The method will perform an AES decryption on given cipher with its nonce, with a given encryption key. The method will return the decrypted message
                :param key: an encryption key
                :type request: bytes class 
                :param msg_data: data to decrypt
                :type request: str and bytes
                :return: a string with the decrypted message
                :rtype: str
    """
    @staticmethod
    def decrypt_data(key, cipher_text, nonce):
        cipher = AES.new(key, AES.MODE_CTR, nonce=nonce)  # creating cipher object
        decrypted_data = cipher.decrypt(cipher_text)  # decrypting the given data

        return decrypted_data
