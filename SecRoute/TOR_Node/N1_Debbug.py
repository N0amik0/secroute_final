# TOR Node (Realy)
 
import sys
import socket
import select
import json
import Connection
import Node_Logic_Unit

SOCKET_LIST = []
CONNECTIONS = {}

RECV_BUFFER = 4096 
MAX_CONNECTIONS = 10

CLIENT_TO_SERVER_DIRECTION = 1
SERVER_TO_CLIENT_DIRECTION = 2

TOR_DIR_ADDR = ('127.0.0.1', 8384)
LOGIC_UNIT = Node_Logic_Unit.Logic_Unit()

def TOR_Node():

    #if(len(sys.argv) < 3) :
    #    print ('Usage : python TOR_Node.py hostname port')
    #    sys.exit()

    IP = '127.0.0.1'#sys.argv[1]
    port = 9001#int(sys.argv[2])


    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as listening_socket:
        listening_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        listening_socket.bind((IP, port))
        listening_socket.listen(MAX_CONNECTIONS)
    
        # add node's master socket object to the list of readable connections
        SOCKET_LIST.append(listening_socket)
    
        print("Node started on port " + str(port))
    
        while True:

            # get the list sockets which are ready to be read through select
            # 4th arg, time_out  = 0 : poll and never block
            ready_to_read, ready_to_write, in_error = select.select(SOCKET_LIST,[],[],0)
        
            for sock in ready_to_read:
                # a new connection request recieved (presuccessors)
                if sock == listening_socket: 
                    connection_sock, addr = listening_socket.accept()
                    SOCKET_LIST.append(connection_sock)
                    CONNECTIONS[connection_sock] = Connection.Connection(CLIENT_TO_SERVER_DIRECTION)
                    
                    # Diffie Hellman!
                    #log - 
                    print("Client (%s, %s) connected" % addr)
                
                # a message from a connected entity, not a new connection
                else:
                    # process data recieved from entity
                    try:
                        data = sock.recv(RECV_BUFFER).decode()
                        print("forwarding: " + data)
                        if data:
                           
                            CONNECTIONS[sock].info = data 
                            # there is something in the socket
                            data = data if CONNECTIONS[sock].direction == SERVER_TO_CLIENT_DIRECTION else json.loads(CONNECTIONS[sock].info)

                            response_json = LOGIC_UNIT.process_data(CONNECTIONS[sock], data)
                            response_str =  response_json["payload"] if CONNECTIONS[sock].is_server and CONNECTIONS[sock].direction == CLIENT_TO_SERVER_DIRECTION else json.dumps(response_json) 
                            
                            if CONNECTIONS[sock].direction != SERVER_TO_CLIENT_DIRECTION and data["successor_IP"] == IP and data["open_port"] == port and data["code"] == 0:
                                sock.send(response_str.encode())

                            elif CONNECTIONS[sock].successor_sock == None:
                                 
                                CONNECTIONS[sock].successor_sock = \
                                    socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                                # TODO: change it to IP not port
                                CONNECTIONS[sock].successor_sock.connect((response_json["successor_IP"], response_json["open_port"])) 

                                SOCKET_LIST.append(CONNECTIONS[sock].successor_sock)
                                CONNECTIONS[CONNECTIONS[sock].successor_sock] = Connection.Connection(SERVER_TO_CLIENT_DIRECTION,
                                    sock, shared_key=CONNECTIONS[sock].shared_key, is_server=CONNECTIONS[sock].is_server)

                            print("Sending: " + response_str)
                            if CONNECTIONS[sock].successor_sock != None:
                                CONNECTIONS[sock].successor_sock.send(response_str.encode())
                            
                        
                        else:
                            # remove the broken socket
                            if sock == TOR_Dir_sock:
                                print("TOR Directory is off")
                                sys.exit()
                                
                            if sock in SOCKET_LIST:
                                SOCKET_LIST.remove(sock)
                                del CONNECTIONS[sock]

                            # at this stage, no data means probably the connection has been broken
                            #log - broadcast(listening_socket, sock, "Client (%s, %s) is offline\n" % addr)
                            print("Client (%s, %s) disconnected" % addr)

                    # exception  
                    except Exception as e:
                        #log - broadcast(listening_socket, sock, "Client (%s, %s) is offline\n" % addr)
                        print(e)
                        print("Client (%s, %s) disconnected" % addr)
                        continue
    
    TOR_Dir_sock.close()

if __name__ == "__main__":

    sys.exit(TOR_Node())


         
 